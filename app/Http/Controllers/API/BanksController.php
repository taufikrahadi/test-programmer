<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Bank;
use App\Http\Resources\BankResource;
use App\Http\Requests\BankRequest;

class BanksController extends Controller
{
    public function index()
    {
        $banks = Bank::latest('created_at')->get();
        return BankResource::collection($banks);
    }

    public function handler($data, $bank)
    {
        $bank->bank_name = $data['bank_name'];
        $bank->contact_email = $data['contact_email'];
        $bank->logo = $data['logo'];
        $bank->save();
        return $bank;
    }

    public function uploadImage($requestLogo)
    {
        $name = time(). '.' .explode('/', explode(':', substr($requestLogo, 0, strpos($requestLogo, ';')))[1])[1];
        $img = \Image::make($requestLogo);
        $img->resize(100, 100);
        $img->save(public_path('logos/').$name);
        return 'logos/'.$name;
    }

    public function store(BankRequest $request)
    {   
        $request->merge(['logo' => $this->uploadImage($request->logo)]);
        $bank = $this->handler($request->all(), new Bank);
        return new BankResource($bank);
    }

    public function show($id)
    {
        $bank = Bank::find($id);
        return new BankResource($bank);
    }

    public function update(Request $request, $id)
    {
        $bank = Bank::find($id);
        $this->validate($request, [
            'bank_name' => 'required|min:3',
            'logo' => 'sometimes|nullable|imageable',
            'contact_email' => 'required|email|unique:banks,contact_email,'.$bank->id,
        ]);

        if(empty($request['logo'])) {
            $request->merge(['logo' => $bank->logo]);
        } else {
            unlink(public_path($bank->logo));
            $request->merge(['logo' => $this->uploadImage($request->logo)]);
        }

        $bank = $this->handler($request->all(), $bank);
        return new BankResource($bank);
    }

    public function destroy($id)
    {
        $bank = Bank::find($id);
        unlink(public_path($bank->logo));
        $bank->delete();
        return response()->json([
            'message' => 'bank deleted',
        ]);
    }
}
