<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('banks', 'API\BanksController@index');
Route::get('banks/{id}', 'API\BanksController@show');
Route::post('banks', 'API\BanksController@store');
Route::patch('banks/{id}', 'API\BanksController@update');
Route::delete('banks/{id}', 'API\BanksController@destroy');
