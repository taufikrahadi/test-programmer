require('./bootstrap');
window.Vue = require('vue');

import { router } from './router'
import App from './components/App'
import VueProgressBar from 'vue-progressbar'
import store from './store'
import { Form, HasError, AlertError } from 'vform'
import Swal from 'sweetalert2'

window.Form = Form
window.Swal = Swal

const options = {
    color: '#0984e3',
    failedColor: '#874b4b',
    thickness: '4px',
    transition: {
      speed: '0.2s',
      opacity: '0.6s',
      termination: 300
    },
    autoRevert: true,
    location: 'top',
    inverse: false
  }
  
    Vue.use(VueProgressBar, options);
    Vue.component(HasError.name, HasError)
    Vue.component(AlertError.name, AlertError)
    Vue.component('bank-form', require('./components/forms/BankForm').default)

const app = new Vue({
    store: store,
    router,
    el: '#app',
    components: { App },
});
