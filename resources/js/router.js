import Vue from 'vue'
import VueRouter from 'vue-router'
import Dashboard from './components/pages/Dashboard'
import BankIndex from './components/pages/BankIndex'
import CreateBank from './components/pages/CreateBank'
import Bank from './components/pages/Bank'
import BankEdit from './components/pages/BankEdit'

Vue.use(VueRouter)

const routes = [
    {
        name: 'dashboard',
        path: '/',
        component: Dashboard,
        icon: 'fa-tachometer-alt',
    },
    {
        name: 'bank',
        path: '/bank',
        component: Bank,
        icon: 'fa-money-check-alt',
        redirect: { name: 'list bank' },
        children: [
            {
                name: 'list bank',
                path: 'index',
                component: BankIndex,
            },
            {
                name: 'create bank',
                path: 'create',
                component: CreateBank,
            },
            {
                name: 'edit bank',
                path: 'edit/:id',
                component: BankEdit,
                props: true,
            }
        ]
    },
]

const router = new VueRouter({
    mode: 'history',
    routes,
})

export {
    router,
    routes
}