import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import Swal from 'sweetalert2'

Vue.use(Vuex)

axios.defaults.baseURL = 'http://localhost:8000/api'

const store = new Vuex.Store({
    state: {
        banks: [],
        isEditing: false,
    },

    mutations: {
        setBanks(state, data) {
            state.banks = data.data
        },

        setIsEditing(state, data) {
            state.isEditing = data
        }
    },

    actions: {
        updateData(context, arg) {
            return new Promise((resolve, reject) => {
                arg.form.patch(`http://localhost:8000/api${arg.url}`).then(response => {
                    context.dispatch('fetchData', { ...arg.fetch })
                    Swal.fire(
                        'Success',
                        'Data Successfully Updated',
                        'success'
                    )
                    resolve(response)
                }).catch(error => {
                    Swal.fire(
                        'Failed',
                        'Failed Updating Data',
                        'error'
                    )
                    reject(error)
                })
            })
        },

        postData(context, arg) {
            return new Promise((resolve, reject) => {
                arg.form.post(`http://localhost:8000/api${arg.url}`).then(response => {
                    context.dispatch('fetchData', { ...arg.fetch })
                    Swal.fire(
                        'Success',
                        'Data Added Successfully',
                        'success'
                    )
                    resolve(response)
                }).catch(error => {
                    Swal.fire(
                        'Failed',
                        'Failed Adding Data',
                        'error'
                    )
                    reject(error)
                })
            })
        },

        deleteData(context, arg) {
            return new Promise((resolve, reject) => {
                axios.delete(arg.deleteUrl)
                    .then((response) => {
                        Swal.fire(
                            'Success',
                            'Data Deleted Successfully',
                            'success'
                        )
                        context.dispatch('fetchData', { ...arg.fetch })
                        resolve(response)
                    })
                    .catch((err) => {
                        Swal.fire(
                            'Failed',
                            'Failed Deleting Data!',
                            'error'
                        )
                        reject(err)
                    })
            })
        },

        fetchData(context, arg) {
            return new Promise((resolve, reject) => {
                axios.get(arg.url).then(response => {
                    context.commit(arg.mut, response.data)
                    resolve(response)
                }).catch(err => {
                    reject(err)
                })
            })
        },

        getDataById(context, arg) {
            return new Promise((resolve, reject) => {
                axios.get(arg.url).then(response => {
                    resolve(response)
                }).catch(err => {
                    reject(err)
                })
            })
        }
    },
})

export default store